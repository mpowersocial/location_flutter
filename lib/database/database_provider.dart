import 'dart:io';
import 'package:path/path.dart' as p;
import 'package:flutter/services.dart';
import 'package:location/constans/table_column.dart';
import 'package:sqflite/sqflite.dart';

int dbVersion = 1;
class DatabaseProvider{
  static final DatabaseProvider dbProvider = DatabaseProvider();
  Database? _database;
  Future<Database?> get database async{
    if(_database != null) return _database;
    _database = await openDb();
    return _database;
  }

  openDb() async {
    var databasesPath = await getDatabasesPath();
    var path = p.join(databasesPath, "location.db");
    /// Check if the database exists
    var exists = await databaseExists(path);

    Database? db=null;

    if (!exists) {
      ///Creating new copy from asset

      /// Copy from asset
      ByteData data = await rootBundle.load('packages/location/assets/location.db');
      List<int> bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      /// Write and flush the bytes written
      await File(path).writeAsBytes(bytes, flush: true);

    } else {
      print("Opening existing database");
    }
   /// open the database
    db = await openDatabase(path, readOnly: true);

    return db;
  }

  void onUpgrade(Database database, int oldVersion, int newVersion) {
    if (newVersion > oldVersion) {
    }
  }


  void initDb(Database database,int version) async{

    ///district table
    await database.execute(
        'CREATE TABLE IF NOT EXISTS $DISTRICT (id INTEGER PRIMARY KEY,'
            '$LOCATION_ID TEXT,'
            '$GEOCODE TEXT,'
            '$FIELD_NAME TEXT,'
            '$FIELD_TYPE_ID TEXT,'
            '$FIELD_PARENT_ID TEXT)'
    );

    ///upazilla table
    await database.execute(
        'CREATE TABLE IF NOT EXISTS $UPAZILA (id INTEGER PRIMARY KEY,'
            '$LOCATION_ID TEXT,'
            '$GEOCODE TEXT,'
            '$FIELD_NAME TEXT,'
            '$FIELD_TYPE_ID TEXT,'
            '$FIELD_PARENT_ID TEXT)'
    );

    ///union table
    await database.execute(
        'CREATE TABLE IF NOT EXISTS $UNION (id INTEGER PRIMARY KEY,'
            '$LOCATION_ID TEXT,'
            '$GEOCODE TEXT,'
            '$FIELD_NAME TEXT,'
            '$FIELD_TYPE_ID TEXT,'
            '$FIELD_PARENT_ID TEXT)'
    );
  }
}