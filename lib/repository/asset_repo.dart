library location;

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:location/constans/table_column.dart';
import 'package:location/database/database_provider.dart';
import 'package:location/models/location_data.dart';
import 'package:sqflite/sqflite.dart';
class LocationRepo{
  ///reset
  void resetTable()async{
    DatabaseProvider  dbProvider = new DatabaseProvider();
    Database? db = await dbProvider.database;
    await db!.delete(DISTRICT);
    await db.delete(UPAZILA);
    await db.delete(UNION);

    String district = await rootBundle.loadString("assets/districts.json");
    String upazilla = await rootBundle.loadString("assets/upazilas.json");
    String union = await rootBundle.loadString("assets/unions.json");

    List<LocationData> districtList = jsonDecode(district);
    List<LocationData> upazillaList = jsonDecode(upazilla);
    List<LocationData> unionList = jsonDecode(union);

    for(var dis in districtList){
      Map<String, dynamic> row = {
        LOCATION_ID:dis.loc_id,
        GEOCODE:dis.geocode,
        FIELD_NAME:dis.fieldName,
        FIELD_TYPE_ID:dis.fieldTypeId,
        FIELD_PARENT_ID:dis.fieldParentId
      };
      await db.insert(DISTRICT, row);
    }

    for(var dis in upazillaList){
      Map<String, dynamic> row = {
        LOCATION_ID:dis.loc_id,
        GEOCODE:dis.geocode,
        FIELD_NAME:dis.fieldName,
        FIELD_TYPE_ID:dis.fieldTypeId,
        FIELD_PARENT_ID:dis.fieldParentId
      };
      await db.insert(UPAZILA, row);
    }

    for(var dis in unionList){
      Map<String, dynamic> row = {
        LOCATION_ID:dis.loc_id,
        GEOCODE:dis.geocode,
        FIELD_NAME:dis.fieldName,
        FIELD_TYPE_ID:dis.fieldTypeId,
        FIELD_PARENT_ID:dis.fieldParentId
      };
      await db.insert(UNION, row);
    }

  }
  ///getting district by id
  Future<LocationData?> getDistrictById(String? id)async{
    DatabaseProvider  dbProvider = new DatabaseProvider();
    Database? db = await dbProvider.database;

    print(await db!.getVersion());
    List<LocationData> location = [];

    var districtRes = await db.rawQuery('select * from $DISTRICT where $LOCATION_ID = "$id"');

    districtRes.forEach((element) {
      location.add(LocationData.fromJson(element));
    });

    return location.length>0?location[0]:null;
  }
  ///getting all district
  Future<List<LocationData>> getDistrict()async{
    DatabaseProvider  dbProvider = new DatabaseProvider();
    Database? db = await dbProvider.database;

    print(await db!.getVersion());
    List<LocationData> location = [];

    var districtRes = await db.rawQuery('select * from $DISTRICT');

    districtRes.forEach((element) {
      location.add(LocationData.fromJson(element));
    });

    return location;
  }

  ///getting all upazila buy district id
  Future<List<LocationData>> getUpazila(String? locId) async{
    DatabaseProvider  dbProvider = new DatabaseProvider();
    Database? db = await dbProvider.database;

    List<LocationData> location = [];

    var upazilaRes = await db!.rawQuery('select * from $UPAZILA where $FIELD_PARENT_ID = "$locId"');

    upazilaRes.forEach((element) {
      location.add(LocationData.fromJson(element));
    });

    return location;
  }
  ///getting all upazila by id
  Future<LocationData?> getUpazilaById(String? id) async{
    DatabaseProvider  dbProvider = new DatabaseProvider();
    Database? db = await dbProvider.database;

    List<LocationData> location = [];

    var upazilaRes = await db!.rawQuery('select * from $UPAZILA where $LOCATION_ID = "$id"');

    upazilaRes.forEach((element) {
      location.add(LocationData.fromJson(element));
    });

    return location.length>0?location[0]:null;
  }

  ///getting all upazila
  Future<List<LocationData>> getAllUpazila() async{
    DatabaseProvider  dbProvider = new DatabaseProvider();
    Database? db = await dbProvider.database;

    List<LocationData> location = [];

    var upazilaRes = await db!.rawQuery('select * from $UPAZILA');

    upazilaRes.forEach((element) {
      location.add(LocationData.fromJson(element));
    });

    return location;
  }

  ///getting all union buy upazila id
  Future<List<LocationData>> getUnion(String? locId) async{
    DatabaseProvider  dbProvider = new DatabaseProvider();
    Database? db = await dbProvider.database;

    List<LocationData> location = [];

    var unionRes = await db!.rawQuery('select * from $UNION where $FIELD_PARENT_ID = "$locId"');

    unionRes.forEach((element) {
      location.add(LocationData.fromJson(element));
    });

    return location;
  }

  ///getting all union by  id
  Future<LocationData?> getUnionById(String? id) async{
    DatabaseProvider  dbProvider = new DatabaseProvider();
    Database? db = await dbProvider.database;

    List<LocationData> location = [];

    var unionRes = await db!.rawQuery('select * from $UNION where $LOCATION_ID = "$id"');

    unionRes.forEach((element) {
      location.add(LocationData.fromJson(element));
    });

    return location.length>0?location[0]:null;
  }

  ///getting all union
  Future<List<LocationData>> getAllUnion() async{
    DatabaseProvider  dbProvider = new DatabaseProvider();
    Database? db = await dbProvider.database;

    List<LocationData> location = [];

    var unionRes = await db!.rawQuery('select * from $UNION');

    unionRes.forEach((element) {
      location.add(LocationData.fromJson(element));
    });

    return location;
  }
}