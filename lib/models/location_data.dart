class LocationData {

  bool operator ==(dynamic other) =>
      other != null && other is LocationData && this.loc_id == other.loc_id;

  @override
  int get hashCode => super.hashCode;

  int? id;
  String? loc_id;
  String? geocode;
  String? fieldName;
  String? fieldTypeId;
  String? fieldParentId;

  LocationData(
      { this.id,
        this.loc_id,
        this.geocode,
        this.fieldName,
        this.fieldTypeId,
        this.fieldParentId});

  LocationData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    loc_id = json['loc_id'].toString();
    geocode = json['geocode']??'';
    fieldName = json['field_name']??'';
    fieldTypeId = json['field_type_id'].toString();
    fieldParentId = json['field_parent_id'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id.toString();
    data['loc_id'] = this.loc_id.toString();
    data['geocode'] = this.geocode??'';
    data['field_name'] = this.fieldName??'';
    data['field_type_id'] = this.fieldTypeId.toString();
    data['field_parent_id'] = this.fieldParentId.toString();
    return data;
  }
}